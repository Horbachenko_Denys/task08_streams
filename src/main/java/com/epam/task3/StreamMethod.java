package com.epam.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamMethod {
    private Supplier<Stream<Integer>> streamSupplier;

    public StreamMethod(List<Integer> ints){
        this.streamSupplier = () -> ints.stream();
    }

    public void printCountStats(){
        Integer maxValue = streamSupplier.get().max(Integer::compare).get();
        Integer minValue = streamSupplier.get().min(Integer::compare).get();
        Double average = streamSupplier.get()
                .mapToInt(integerList -> integerList)
                .average()
                .orElse(0.0);

        Integer sumReduce = streamSupplier.get().reduce(0, Integer::sum);
        Integer sumMethod = streamSupplier.get().mapToInt(Integer::intValue).sum();
        System.out.println("Max: " + maxValue);
        System.out.println("Min: " + minValue);
        System.out.println("Average: " + average);
        System.out.println("Sum by reduce : " + sumReduce);
        System.out.println("Sum by method: " + sumMethod);

    }

    public void sumBiggerThanAverage(){
        Double average = streamSupplier.get()
                .mapToInt(integerList -> integerList)
                .average()
                .orElse(0.0);

        Integer sum = streamSupplier.get()
                .filter(i -> i > average)
                .mapToInt(i -> i )
                .sum();

        System.out.println("Sum of elements bigger than averange: " + sum);
    }

}
