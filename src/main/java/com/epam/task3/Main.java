package com.epam.task3;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Stream<Integer> streamList = integers.stream();
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Stream<Integer> stream1 = Stream.of(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        Stream<Integer> stream2 = Stream.iterate(1, n -> n + 1).limit(11);
        Stream<Integer> stream3 = Stream.generate(new Random()::nextInt);

        StreamMethod streamMethod = new StreamMethod(integers);
        streamMethod.printCountStats();
        streamMethod.sumBiggerThanAverage();

    }
}
