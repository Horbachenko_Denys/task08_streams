package com.epam.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);
    private static final String filePath = "./result task1.txt";

    public static void main(String[] args) {

        try(FileWriter fw = new FileWriter(filePath)){
       Addeble addeble = ((a, b, c) -> Math.max(Math.max(a,b), c));
        fw.write(addeble.add(1, 3, 7) + "\n") ;
        addeble = ((a, b, c) -> (a + b + c) / 3);
            fw.write(addeble.add(2, 4, 6) + "\n");
        }
        catch (IOException e){
            logger.error("IO Error");
        }
    }
}
