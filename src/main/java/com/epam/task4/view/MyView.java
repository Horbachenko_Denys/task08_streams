package com.epam.task4.view;

import com.epam.task4.controller.Controller;
import com.epam.task4.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - return number of unique words");
        menu.put("2", " 2 - return sorted list of unique words");
        menu.put("3", " 3 - count numbers of word");
        menu.put("4", " 4 - count occurrence of each symbol");
        menu.put("Q", " Q - Exit");

        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void enterWords() {
        System.out.println(controller.enterWords());
    }

    private void pressButton1() {
        System.out.println("List of unique words:");
        System.out.println(controller.getListUniqueWords());
    }

    private void pressButton2() {
        System.out.println("Sorted list of unique words:");
        System.out.println(controller.getSortedListUniqueWords());
    }

    private void pressButton3() {
        System.out.println("Numbers of word:");
        System.out.println(controller.getNumbersWords());
    }

    private void pressButton4() {
        System.out.println("Numbers of each symbol:");
        System.out.println(controller.getNumbersOfSymbols());
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        enterWords();
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in menu");
            }
        } while (!keyMenu.equals("Q"));
    }
}
