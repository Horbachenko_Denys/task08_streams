package com.epam.task4.view;

public interface Printable {

    void print();
}
