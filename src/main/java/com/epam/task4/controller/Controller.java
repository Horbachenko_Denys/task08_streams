package com.epam.task4.controller;

import java.util.List;
import java.util.Map;

public interface Controller {

    List<String> enterWords();

    List<String> getListUniqueWords();

    List<String> getSortedListUniqueWords();

    Map<String, Long> getNumbersWords();

    Map<Character, Long> getNumbersOfSymbols();
}