package com.epam.task4.controller;

import com.epam.task4.model.Logic;
import com.epam.task4.model.Model;

import java.util.List;
import java.util.Map;

public class ControllerImpl implements Controller{
    private Model model;

    public ControllerImpl() {
        model = new Logic();
    }

    @Override
    public List<String> enterWords() {
        return  model.enterWords();
    }

    @Override
    public List<String> getListUniqueWords() {
        return model.getListUniqueWords();
    }

    @Override
    public List<String> getSortedListUniqueWords() {
        return model.getSortedListUniqueWords();
    }


    @Override
    public Map<String, Long> getNumbersWords() {
        return model.getNumbersWords();
    }

    @Override
    public Map<Character, Long> getNumbersOfSymbols() {
        return model.getNumbersOfSymbols();
    }
}

