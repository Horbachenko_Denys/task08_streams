package com.epam.task4.model;

import java.util.List;
import java.util.Map;

public class Logic implements Model {

    private MyList myList ;

    public Logic (){
        myList = new MyList();
    }
    @Override
    public List<String> enterWords() {
        return myList.enterWords();
    }

    @Override
    public List<String> getListUniqueWords() {
        return myList.getListUniqueWords();
    }

    @Override
    public List<String> getSortedListUniqueWords() {
        return myList.getSortedListUniqueWords();
    }

    @Override
    public Map<String, Long> getNumbersWords() {
        return myList.getNumbersWords();
    }

    @Override
    public Map<Character, Long> getNumbersOfSymbols() {
        return myList.getNumbersOfSymbols();

    }
}
