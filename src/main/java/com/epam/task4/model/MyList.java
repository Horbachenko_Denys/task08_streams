package com.epam.task4.model;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MyList {

    private static Scanner input = new Scanner(System.in);
    private List<String> enteredList = new ArrayList<>();
    private List<String> uniqueWordsList = new ArrayList<>();
    Map<Character, Long> symbolCount;


    public List<String> enterWords() {
        String in;
        do{
        System.out.println("Please, enter word...");
        in = input.nextLine();
        enteredList.add(in);
    } while (!in.equals(""));
        return enteredList;
    }

    public List<String> getListUniqueWords() {
        return uniqueWordsList = enteredList.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> getSortedListUniqueWords() {
        getListUniqueWords();
        return uniqueWordsList
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> getNumbersWords() {
        return enteredList
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public Map<Character, Long> getNumbersOfSymbols() {
        return symbolCount = enteredList.stream()
                .flatMap(s -> s.chars().mapToObj(c -> (char) c))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
